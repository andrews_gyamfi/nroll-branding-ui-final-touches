/** 
 * Simple Task Runner for Landing Page Creation
 * Tasks: 
 * 1. Image minimization
 * 2. Compiling less into css 
 * 3. CSS minification 
 * 4. CSS source maps
 */
var gulp = require("gulp"),
    browserSync = require("browser-sync").create(),
    minifyCss = require("gulp-minify-css"),
    sourcemaps = require("gulp-sourcemaps"),
    less = require("gulp-less"),
    uglify = require("gulp-uglify"),
    concat = require("gulp-concat"),
    ngAnnotate = require("gulp-ng-annotate"),
    nodemon = require('gulp-nodemon');


/**
 * Tasks to compile less file and minify it
 */
gulp.task("minifyCss", function () {
    gulp.src("./public/less/style.less")
        .pipe(less())
        .pipe(sourcemaps.init())
        .pipe(minifyCss())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest("./public/dist"))
        .pipe(browserSync.stream);
});

/**
 * Task to minify angular modules into one
 */
gulp.task("minifyJs", function () {
    gulp.src(["./frontend/app.js", "./frontend/app/**/*.js"])
        .pipe(sourcemaps.init())
        .pipe(concat("main.js"))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest("./frontend/dist/js"));
})

/**
 * Default Task function 
 */
gulp.task("default", ["minifyJs"], function () {
    nodemon({
            script: "./index.js",
            ext: "js",
            ignore: ["./node_modules"],
        })
        .on("restart", function () {
            console.log("Restarted server...")
        });

    /* Initialise browsersync */
    browserSync.init({
        server: "./",
        browser: "chrome",
        port: "4000"
    });

    /* Watch file changes */
    gulp.watch("./public/*.html", browserSync.reload);
    gulp.watch("./public/less/**/*.less", browserSync.reload);
    gulp.watch("./public/less/**/*.less", browserSync.reload);
    gulp.watch("/frontend/app/**/*.js", ["minifyJs", browserSync.reload]);
});