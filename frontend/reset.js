var resetApp = angular.module("resetPasswordApp", []);

resetApp.controller("resetPasswordCtrl", ["$scope", "$location", "$http", "$location", "$window", function ($scope, $location, $http, $location, $window) {
	/* Scope reference to password */
	$scope.formData = {
		password: "",
		cPassword: ""
	};

	var rawUrl = $location.absUrl(),
		token = rawUrl.split("=")[1];

	$scope.resetPassword = function () {
		$http.post("/api/auth/newpassword", {
				token: token,
				newPassword: $scope.formData.password,
				cNewPassword: $scope.formData.cPassword
			})
			.then(function (response) {
				if (response.status === 200) {
					$window.location.href = "https://apply.peg.edu.au";
				}
			})
			.catch(function (response) {

			})
	}

}]);