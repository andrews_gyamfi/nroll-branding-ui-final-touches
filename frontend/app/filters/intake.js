/**
 * Simple filter to select unique courses from API intakes
 * This is used on the course selection page
 */
angular.module("nroll")

.filter("intake", function () {
	/* Return the filtering function */
	return function (collection, campus) {

		/* Variable to hold filtered output array */
		var output = [];

		/* Check for null or error values */
		if (!angular.isArray(collection)) {
			return;
		}

		/* If campus is not defined, return */
		if (!campus || campus === "") {
			return;
		}

		/* Run a foreach loop and filter intakes based by chosen campus */
		_.forEach(collection, function (collection) {
			if (collection.campus.id === campus.id) {
				output.push(collection);
			}
		})

		output = _.sortBy(output, function (o) {
			return o.startDate;
		});
		/* Return filtered output array */
		return output;
	}

})