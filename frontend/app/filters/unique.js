/**
 * Simple filter to select unique courses from API intakes
 * This is used on the course selection page
 */
angular.module("nroll")

.filter("unique", function () {
    /* Return the filtering function */
    return function (collection, keyname) {
        /* Variable reference for finalized array */
        var output = [],
            /* Variable referenec to temporary store keys */
            keys = [],
            firstKeyName,
            secondKeyName;

        if (keyname.indexOf(".")) {
            var splitKeys = keyname.split(".");
            firstKeyName = splitKeys[0];
            secondKeyName = splitKeys[1];
        }

        /* Loop through collection and only store only unique values */
        angular.forEach(collection, function (item) {
            /* Get intake course's Id*/
            var key = item[firstKeyName][secondKeyName];

            /* If course Id does not already exist in key array, push it onto array*/
            if (keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        /* Return final array */
        return output;
    }

})