/**
 * Custom Date Validator
 */

angular.module("nroll")
	.directive("dateValidate", function () {
		var dateTypes = ["dob", "year"],
			yearRegex = /(?:(?:19|20)[0-9]{2})/;

		/** 
		 * Link function definition 
		 */
		var link = function (scope, elem, attr, ngModel) {
			/* Declare variable to hold validation type */

			/* Get the validation type from element */
			var dateType = attr.dateType,
				regex = new RegExp(yearRegex);

			/* Declare and attach new validation function */
			ngModel.$validators.dateValidate = function (modelValue, viewValue) {
				var value = modelValue || viewValue;

				/* If field is not required, do not validate */
				attr.$observe("required", function (value) {
					if (!value) {
						ngModel.$setValidity("dateValidate", true);
					}
				});

				if (dateType === "year") {
					return regex.test(value) && parseInt(moment(new Date(value)).format("YYYY")) <= parseInt(moment(new Date()).format("YYYY")) && !(parseInt(moment(new Date(value)).format("YYYY")) < 1940);

				} else if (dateType === "dob") {
					return parseInt(moment(new Date(value)).toNow().split(" ")[1]) > 16 && !(parseInt(moment(new Date(value)).toNow().split(" ")[1]) > 60);
					console.log(parseInt(moment(new Date(value)).toNow().split(" ")[1]));
				} else {
					return false;
				}

			};

		};


		/*Return directive definition object */
		return {
			restrict: "A",
			require: "ngModel",
			scope: false,
			link: link
		};
	})