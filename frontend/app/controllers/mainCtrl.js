/**
 * Main controller handles login and logout
 */
angular.module("nroll")

.controller("mainCtrl", ["TokenFactory", "AuthFactory", "$state", "$rootScope", "$log", "User", "$auth", "AUTH_EVENTS", "UI_ROUTER_EVENTS", function (TokenFactory, AuthFactory, $state, $rootScope, $log, User, $auth, AUTH_EVENTS, UI_ROUTER_EVENTS) {

    /* Declare view-model variable - vm */
    var vm = this;

    /* User login object */
    vm.user = {
        email: "",
        password: ""
    };

    /* Loading animation boolean flag */
    vm.isLoading = $rootScope.stateIsLoading;

    /* Current User */
    vm.currentUser = $rootScope.currentUser;


    /* Login failed flag */
    vm.loginFailed = false;

    /**
     * Method initializes app if authenticated
     */
    var initApp = function (user) {
        /* Store current user information in $rootScope */
        $rootScope.currentUser = user;
        $rootScope.authenticated = true;

        $rootScope.$broadcast(AUTH_EVENTS.LOGGEDIN);

        /* Change state to dashboard */
        $state.go("app.home");
    }

    /**
     * Method attempts to login user through api endpoint 
     */
    vm.login = function () {

        /* Attempt login */
        $auth.login({
                email: vm.user.email,
                password: vm.user.password
            })
            /* Process success */
            .then(function (response) {
                /* Direct user to homepage */
                if (response.status === 200) {
                    initApp(response.data.user);
                }

            })
            /* Process error */
            .catch(function (response) {
                /* Notify user - unsuccessful */
                if (response.status === 401) {
                    vm.loginFailed = true;
                }
            });
    }



    /**
     * Method logs out user via AuthFactory 
     * Redirects user to login page after logout
     */
    vm.logout = function () {
        $auth.logout();
        $rootScope.authenticated = false;
        $state.go("login");

        /*
        AuthFactory.logout();
        if (!AuthFactory.loggedIn) {
            $state.go("login");
        } 
        */
    }

    /**
     * Method gets basic user information from api 
     */
    vm.getUser = function () {
        AuthFactory.getUser()
            .success(function (response) {
                console.log(response);
            })
    }

}]);