/**
 * Header Controller
 * Responsible for showing and hiding relevant navigation links 
 * based on user's login status
 */
angular.module("nroll")

/* Requires AuthFactory */
.controller("headerCtrl", ["$rootScope", "AUTH_EVENTS", function ($rootScope, AUTH_EVENTS) {
    /* Declare vm variable */
    var vm = this;

    /* Deckare variable to store user's name */
    vm.userName = $rootScope.currentUser.firstName;

    /* Declare variable for login status */
    vm.loginStatus = $rootScope.authenticated;

    /* Watch $rootScope authenticated property for changes */
    $rootScope.$watch(function () {
        return $rootScope.authenticated;
    }, function (value) {
        setValues();
    })

    /**
     * Method initializes userName and loginStatus 
     */

    var setValues = function () {
        vm.loginStatus = $rootScope.authenticated;
        vm.userName = ucFirst($rootScope.currentUser.firstName);
    }


    /**
     * Method capitalizes first letter of string
     */
    var ucFirst = function (name) {
        if (!name) {
            return;
        }
        return name.charAt(0).toUpperCase() + name.slice(1);
    }

}]);