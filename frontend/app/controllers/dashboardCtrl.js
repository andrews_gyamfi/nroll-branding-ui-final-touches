/**
 * Dashboard Controller
 */
angular.module("nroll")

.controller("dashboardCtrl", ["User", "EnrolmentManager", "myapplications", '$state', function (User, EnrolmentManager, myapplications, $state) {

	var vm = this;

	/* Welcome Message */
	vm.welcomeMessage = "You currently do not  have any applications.";

	/* Set if applicant has applications in progress */
	vm.myapplications = myapplications;

	/* Saved Applications */
	vm.savedApplications = [];

	/* Completed Applications */
	vm.completedApplications = [];

	/* Boolean flag - has application */
	vm.hasApplications = Boolean(myapplications.length) ? true : false;

	vm.resumeApp = function (index) {
		var appId = vm.myapplications[index].appRefId
		EnrolmentManager.resume(appId)
			.then(function (response) {
				EnrolmentManager.setCurrentApplication(response.data.application);
				$state.go("app.apply.selectCourse");
			})
			.catch(function (response) {})
	}
}]);