/**
 * User controller handles user object
 */
angular.module("nroll")

/* Requires: User factory */
.controller("userCtrl", ["$rootScope", "UserFactory", "$state", "User", "$auth", function ($rootScope, UserFactory, $state, User, $auth) {
	/* Set vm as view-model variables */
	var vm = this;

	/* User basic model */
	vm.regUser = {
		firstName: "",
		lastName: "",
		email: "",
		password: "",
		consultationId: ""
	};

	/* Error message */
	vm.errorMessage = "";


	/* Registration response flag */
	vm.registerFailed = false;

	/**
	 * Method creates new user
	 */
	vm.createUser = function () {

		$auth.signup(vm.regUser)
			.then(function (response) {
				/* Store current user information in $rootScope */
				$rootScope.currentUser = response.data.user;
				$rootScope.authenticated = true;

				$auth.setToken(response.data.token);

				/* Change state to dashboard */
				$state.go("app.home");
			})
			.catch(function (response) {
				console.log(response);
				if (response.status === 400 && !response.data.succces) {
					vm.errorMessage = response.data.message;
				}
				vm.registerFailed = true;
			})
	}

}]);