/**
 * Module handles course selection
 */

angular.module("nroll")

/* Requires: User model to function properly */

.controller("applicationCtrl", ["$rootScope", "$scope", "EnrolmentManager", "intakes", "countries", "referenceData", "$uibModal", "wizardManager", "countries", "languages", "Upload", "ModalService", "$state", function ($rootScope, $scope, EnrolmentManager, intakes, countries, referenceData, $uibModal, wizardManager, countries, languages, Upload, ModalService, $state) {

	/* set application in progress on $rootScope */
	$rootScope.applicationInProgress = true;

	/* Academy & Industry map */
	var industryAcademyMap = {
		"Beauty": "National Academy of Beauty",
		"Childhood": "National Academy of Early Childhood",
		"IT": "National Academy of Technology",
		"Business": "Australian Institute of Business Leadership"
	}


	/* Set view model variable */
	var vm = this;

	/* Create new enrolment manager */
	vm.enrolmentManager = EnrolmentManager;

	/* Get reference to current application instance */
	vm.applicant = vm.enrolmentManager.getApplication();

	/* Active Form */
	vm.activeForm = vm.applicant.activeForm;

	/* Set modal options */
	vm.modalOptions = {
		animation: true,
		templateUrl: "../../views/modals/cancelEnrolment",
		controller: "choicesCtrl"
	}

	/* Flag for attempts to move next step */
	vm.attemptedNext = false;

	/* Panels */
	vm.panels = {
		personalDetails: true,
		contactDetails: true,
		educationDetails: true,
		otherDetails: true
	}

	/* Set email address to that of current user */
	vm.applicant.contactInfo.email = $rootScope.currentUser.email;

	/* Assign referenceData */
	vm.references = referenceData.refData;

	/* Assign countries to variable */
	vm.countries = countries;

	/* Assign languages to variable */
	vm.languages = languages;


	/* Assign intakes to model */
	vm.intakes = intakes;

	/* Assign Reference Data */
	vm.references = referenceData.refData;
	/* Assign countries to model*/
	vm.countries = countries;

	/* Assign extracted courses to mdoel */
	vm.courses = [];

	/* course choices */
	vm.courseChoices = vm.applicant.courseChoices;

	/* selected course */

	/* Filtered courses - all duplicates removed */
	vm.filteredCourses = [];

	/* Unique courses */
	vm.uniqueCourses;

	/* Course choices */

	/* Intake relevant to chosen course */
	vm.currentIntakes = [];

	/* Campuses relevant to chosen course */
	vm.currentCampuses = [];

	/* Maximum courses allowed */
	vm.maxAllowedCourses = 1;

	/* Maximum courses reached flag */
	vm.maxAllowedReached = vm.applicant.courseLimitReached();

	/* Flag for choices - to be used to show/hide elements on page */
	vm.hasChoices = vm.applicant.hasChoices();


	/* Flags for Document upload section collapse divs */
	vm.isVTGCollapsed = false;
	vm.isVFHCollapsed = false;

	/* Declare view-model variable and assign it to this scope */
	var vm = this;

	/* Reference to Applicant object */
	vm.files = [];


	/**
	 * Method extract courses from massive intakes object
	 */
	var extractCourses = function (intakes) {
		_.filter(intakes, function (intake) {
			vm.courses.push({
				id: intake.Course__r.Id,
				code: intake.Course__r.ProductCode,
				name: intake.Course__r.Name,
				industry: intake.Course__r.Industry__c,
			});
		});
	}

	/* invoke function */
	extractCourses(vm.intakes);

	/**
	 * Method filters courses by removing duplicates
	 */
	var filterCourses = function () {
		vm.filteredCourses = _.uniqBy(vm.courses, function (course) {
			return course.id;
		});

	}

	/* invoke function to filter courses */
	filterCourses();

	/**
	 * Method filters campuses by removing duplcates 
	 */
	var filterCampuses = function () {

		var allCampuses = [];

		_.filter(vm.currentIntakes, function (intake) {
			allCampuses.push(intake.campus);
		})

		vm.currentCampuses = _.uniqBy(allCampuses, function (campus) {
			return campus.id;
		});

	}

	/* Get All intake dates */
	var getIntakes = function (index) {

		/* Reset intakes and campus choices */
		resetInput(vm.applicationForm.coursesForm.campusChoice);
		resetInput(vm.applicationForm.coursesForm.intakeChoice);

		vm.courseChoices.campus = "";
		vm.courseChoices.intake = "";

		/* Get local reference to course object */
		var course = index;

		/* Get relevant academy related to course */
		vm.applicant.academy = industryAcademyMap[course.industry];

		/* Error checking */
		if (!course) {
			return;
		}

		/* Reset intake array */
		if (vm.currentIntakes.length > 0) {
			vm.currentIntakes = [];
		}

		/* Find all intakes relevant to selected course */
		_.filter(vm.intakes, function (intake) {
			if (intake.Course__r.Id === course.id) {
				vm.currentIntakes.push({
					id: intake.Id,
					startDate: new Date(intake.Online_Orientation_Day__c),
					courseId: intake.Course__r.Id,
					industry: intake.Course__r.Industry__c,
					campus: {
						id: intake.Campus__r.Id,
						name: intake.Campus__r.Name
					}
				});
			}
		});

		filterCampuses();

	}

	/* Method resets input */
	var resetInput = function (input) {
		input.$setUntouched();
	}


	/* Method 'nullifies value */
	var nullify = function (model) {
		model = "";
	}

	/* Method resets start date input */
	var resetStartDate = function () {
		resetInput(vm.applicationForm.coursesForm.intakeChoice);
		vm.courseChoices.intake = "";
	}

	/**
	 * Method returns TRUE if study reason is 'Other'
	 */
	var showOtherReason = function () {
		return vm.applicant.otherInfo.studyReason === "Other"
	}

	/**
	 * Method to check if visa status should be displayed
	 */
	var showVisaStatus = function () {
		if (vm.applicant.citizenship.country === null || vm.applicant.citizenship.country === "") {
			return false;
		}

		if (vm.applicant.citizenship.country === "New Zealand" && vm.applicant.citizenship.scvVisa === null) {
			return false;
		}


		if (vm.applicant.citizenship.country === "New Zealand" && !vm.applicant.citizenship.scvVisa) {
			return true;
		}


		if (vm.applicant.citizenship.country === "Australia" || vm.applicant.citizenship.country === "New Zealand") {
			return false;
		}


		return true;
	}

	/**
	 * Method to reveal SCV visa section if New Zealand citizen
	 */
	var showScvVisa = function () {
		return vm.applicant.citizenship.country === "New Zealand";
	}

	/**
	 * Method returns boolean if New Zealand citizen has SCV Visa or not
	 */
	var hasScvVisa = function () {
		return vm.applicant.citizenship.scvVisa;
	}


	/* Method determines VFH eligibility */
	var vfhEligible = function () {
		if (vm.applicant.citizenship.country === "Australia" && getCourseLevel() >= 5) {
			return true;
		}

		if (vm.applicant.citizenship.country === "New Zealand" && hasScvVisa() && getCourseLevel() >= 5) {
			return true;
		}

		if ((vm.applicant.citizenship.country !== "Australia" || vm.applicant.citizenship.country !== "New Zealand") && vm.applicant.citizenship.visaStatus === "Permanent Humanitarian Visa Holder" && getCourseLevel() >= 5) {
			return true;
		}

		return false;
	}


	/**
	 * Method checks to see if other visa section should be displayed
	 */

	var showOtherVisa = function () {

		if (vm.applicant.citizenship.country === "New Zealand" && !vm.applicant.citizenship.scvVisa && vm.applicant.citizenship.visaStatus === 'Other Visa') {
			return true;
		}


		if (vm.applicant.citizenship.country === "Australia" || vm.applicant.citizenship.country === "New Zealand") {
			return false;
		}

		if (vm.applicant.citizenship.visaStatus === 'Other Visa') {
			return true;
		}
		return false;
	}


	/* Method returns boolean depending on whether student is accessing VFH or not */
	var accesingVfh = function () {
		return vm.applicant.funding.vfh.wantsToAccess;
	}

	/* Method returns boolean depending on whether student has prior education with a Higher Education Provider or RTO */
	var hasPriorTraining = function () {
		return vm.applicant.funding.vfh.hasRecentProvider;
	}

	/* Method returns boolean: depending on whether applicant has accessed VFH previously */
	var hasAccessedVfhBefore = function () {
		return vm.applicant.funding.vfh.previouslyAccessedVfh;
	}


	/* Cancellation modal */
	var cancellationModal = function () {
		$rootScope.applicationInProgress = !$rootScope.applicationInProgress;
		ModalService.quitNewApplication("app.home");
	};

	/* Method checks to make sure at least course has been selected before proceeding 
	if (vm.applicant.getChoicesLength !== 1) {
	    vm.coursesForm.$setValidity(false);
	}
	*/

	/* Persist object in localStorage */
	var persist = function () {
		vm.enrolmentManager.serializeCurrentApplication();
	}

	/* Method returns whether school state is VIC or not */
	var schoolStateIsVic = function () {
		return vm.applicant.educationInfo.highSchool.state === "VIC";
	}

	/* Method returns whether student has VSN */
	var hasVsn = function () {
		return vm.applicant.educationInfo.hasVsn
	}

	/* Method returns boolean: depending on hasVsn value */
	var noVsn = function () {
		if (vm.applicant.educationInfo.hasVsn === null) {
			return false;
		};

		if (!vm.applicant.educationInfo.hasVsn && vm.applicant.educationInfo.highSchool.state === "VIC") {
			return true;
		};
	}


	/* Method returns boolean: depending on whether student attended VET school */
	var attendedVet = function () {
		return vm.applicant.educationInfo.attendedVet && !hasVsn();
	}

	/* Method returns boolean: depending on whether student attended Vic school */
	var attendedVicSchool = function () {
		return vm.applicant.educationInfo.highSchool.state === "VIC" && vm.applicant.educationInfo.attendedVicSchool;
	}


	/* Method returns boolean: depending on selected employment status */
	var isEmployed = function () {
		return vm.applicant.employmentInfo.employmentStatus === "Full time employee" || vm.applicant.employmentInfo.employmentStatus === "Part time employee";

	}

	/* Method returns boolean: depending on whether applicant select other disability */
	var hasOtherDisability = function () {
		return vm.applicant.otherInfo.disabilityType === "Other";
	}

	/* Method returns boolean: depending on whether applicant selected has disability management plan or not */
	var hasDisabilityPlan = function () {
		return vm.applicant.otherInfo.hasDisabilityPlan;
	}

	/* Method collapses accordion */
	var toggleCollapse = function (panel) {
		panel = !panel;
	}

	/* Method returns boolean based on language spoken*/
	var languageNotEnglish = function () {
		if (vm.applicant.otherInfo.language === "") {
			return false;
		}

		return vm.applicant.otherInfo.language !== "English";
	}

	/* Method returns boolean if student lives in QLD */
	var livesInQld = function () {
		return vm.applicant.contactInfo.address.residential.state === 'QLD';
	}


	/* Method returns boolean if student is still attending secondary school */
	var stillAttendingSec = function () {
		return vm.applicant.educationInfo.stillAttending;
	}

	/* Method returns boolean if student is still attending year twelve*/
	var attendingYearTwelve = function () {
		return vm.applicant.educationInfo.attendingYearTwelve;
	}

	/* Method returns boolean if student has disability */
	var hasDisability = function () {
		return vm.applicant.otherInfo.isDisabled;
	}

	var attendedSecondarySchool = function () {
		if (vm.applicant.educationInfo.highestLevel === '') {
			return false;
		}

		if (vm.applicant.educationInfo.highestLevel !== 'Never Attended School') {
			return true;
		}

		return false;
	}

	/* Method returns boolean if student is not born in Australia */
	var notBornInAustralia = function () {
		if (vm.applicant.personalInfo.countryOfBirth === "") {
			return false;
		}


		if (vm.applicant.personalInfo.countryOfBirth !== 'Australia') {
			return true;
		}

		return false;
	}

	/* Method returns boolean depending on whether student has previous qualification or not*/
	var hasPreviousQual = function () {
		if (vm.applicant.educationInfo.previousQuals !== "") {
			return true;
		}
		return false;
	}

	/* Method returns boolean if student is under 18yrs or not */
	var underEighteen = function () {
		return parseInt(moment(new Date(vm.applicant.personalInfo.dateOfBirth)).toNow().split(" ")[1]) < 18;

	}

	/* Add selected file to list of files to be uploaded */
	var addFile = function (file) {

		/* Fix for Upload bug where a null object is returned before file is even selected*/
		if (file === null) {
			return false;
		} else if (file) {
			/* add files to array of files - these will be uploaded on completion of the form*/
			vm.files.push(file);
		}
	};

	/* Method removes selected file */
	var removeFile = function (index) {
		_.pullAt(vm.files, index);
	}


	/* STEP NAGIVATION METHODS */
	var gotoNextStep = function (form, nextStepId) {
		if (!form.$valid) {
			vm.attemptedNext = true;
			return;
		}

		if (nextStepId === 1) {
			/* Serialize form */
			persist();
		}
		vm.applicant.activeForm = nextStepId;
	}

	/* Go to specific step */
	var gotoStep = function (stepId) {
		vm.applicant.activeForm = stepId;
	}

	/* Set step */
	var setStep = function (stepId) {
		vm.applicant.activeForm = stepId;
	}

	/* Get course level */
	var getCourseLevel = function () {
		if (!vm.applicant.courseChoices.course || vm.applicant.courseChoices.course.code === "") {
			return;
		}
		return parseInt(vm.applicant.courseChoices.course.code.charAt(3));
	}


	/* Final submit function */
	var submit = function () {

		/* Disable the apply button */
		vm.buttonDisable = true;

		/* Attempt sending application details to API */
		vm.enrolmentManager.apply()
			.then(function (response) {
				if (response.status === 200 || response.data.success) {

					/* If application details is successful, attempt document uploads */
					Upload.upload({
						url: "api/upload/" + response.data.id,
						data: {
							files: vm.files
						}

					}).then(function (response) {
						if (response.status === 200 || response.data.success) {
							/* Show success Modal */
							ModalService.successfulSubmission();
						};
					});

				}
			}).catch(function (response) {
				if (response.status === 400 || !response.data.succces) {
					ModalService.errorSubmission();
				}
				/* If wasn't successful, enable the apply button again */
				vm.buttonDisable = false;
			})
	};

	vm.underEighteen = underEighteen;
	vm.submit = submit;
	vm.resetStartDate = resetStartDate;


	vm.gotoNextStep = gotoNextStep;
	vm.gotoStep = gotoStep;
	vm.setStep = setStep;
	/* Expose functions */
	vm.removeFile = removeFile;
	vm.cancellationModal = cancellationModal;
	vm.addFile = addFile;


	/* Expose methods on view-model variable */
	//	vm.addCourseChoice = addCourseChoice;
	//	vm.removeCourseChoice = removeCourseChoice;
	vm.getIntakes = getIntakes;
	vm.showVisaStatus = showVisaStatus;
	vm.showOtherVisa = showOtherVisa;
	vm.showOtherReason = showOtherReason;
	vm.showScvVisa = showScvVisa;
	vm.hasScvVisa = hasScvVisa;
	vm.vfhEligible = vfhEligible;
	vm.accesingVfh = accesingVfh;
	vm.hasPriorTraining = hasPriorTraining;
	vm.hasAccessedVfhBefore = hasAccessedVfhBefore;
	vm.persist = persist;

	/* Expose methods */
	vm.schoolStateIsVic = schoolStateIsVic;
	vm.hasVsn = hasVsn;
	vm.attendedVet = attendedVet;
	vm.attendedVicSchool = attendedVicSchool;
	vm.noVsn = noVsn;
	vm.isEmployed = isEmployed;
	vm.hasOtherDisability = hasOtherDisability;
	vm.hasDisabilityPlan = hasDisabilityPlan;
	vm.toggleCollapse = toggleCollapse;
	vm.languageNotEnglish = languageNotEnglish;
	vm.livesInQld = livesInQld;
	vm.stillAttendingSec = stillAttendingSec;
	vm.attendingYearTwelve = attendingYearTwelve;
	vm.hasDisability = hasDisability;
	vm.attendedSecondarySchool = attendedSecondarySchool;
	vm.notBornInAustralia = notBornInAustralia;
	vm.hasPreviousQual = hasPreviousQual;


}]);