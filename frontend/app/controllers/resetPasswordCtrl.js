/**
 * Controller handles password reset
 */
angular.module("nroll")

.controller("resetPasswordCtrl", function ($http) {
	/* Declare vm variable */
	var vm = this;

	/* Email */
	vm.email;

	/* Results flag */
	vm.emailSent = false;

	/* Send http request to API */
	vm.sendEmail = function () {

		$http.post("/api/auth/reset", {
				email: vm.email
			})
			.then(function (response) {
				if (response.status === 200) {
					vm.emailSent = true;
				}
			})
			.catch(function (response) {
				console.log(response);
			})
	}

})