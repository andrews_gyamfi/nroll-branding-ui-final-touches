/**
 * App Controller
 */
angular.module("nroll")

/* Requires AuthFactory */
.controller("appCtrl", ["$state", function ($state) {


	/* View-model variable */
	var vm = this;

	/* Flag for navigation toggel */
	vm.toggleNavFlag = false;

	/* Method toggles navigation when called*/
	var toggleNav = function () {
		vm.toggleNavFlag = !vm.toggleNavFlag;
	}

	/* Expose methods for external use */
	vm.toggleNav = toggleNav;

}]);