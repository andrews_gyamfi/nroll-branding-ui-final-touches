/**
 * ApplicationFactory models the application form
 */
angular.module("nroll")

.factory("Applicant", ["$http", "$q", function ($http, $q) {
	/* Declare Applicant's personal information model */

	function Applicant() {

		this.personalInfo = {
			title: "",
			firstName: "",
			lastName: "",
			previousFirstName: "",
			previousLastName: "",
			dateOfBirth: "",
			countryOfBirth: "",
			culturalBackground: "",
			yearOfArrival: "",
			gender: "",
			hasPreviousName: null
		};

		/* Citizenship status */
		this.citizenship = {
			country: "",
			visaStatus: null,
			otherVisa: {
				subCategory: "",
				expiryDate: "",
				allowedToStudy: null
			},
			scvVisa: null
		}

		/* Course Funding */
		this.funding = {
			vfh: {
				eligible: null,
				wantsToAccess: null,
				hasRecentProvider: null,
				recentProvider: {
					name: "",
					yearLastEnrolled: "",
					studentNumber: ""
				},
				previouslyAccessedVfh: null,
				CHESSN: ""
			}
		}

		/* Declare Applicant's contact information model */
		this.contactInfo = {
			mobile: "",
			email: "",
			parentEmail: "",
			address: {
				same: null,
				residential: {
					street: "",
					suburb: "",
					state: "",
					postcode: ""
				},
				postal: {
					street: "",
					suburb: "",
					state: "",
					postcode: ""
				}
			}
		};

		/* Declare emergency contact model */
		this.emergencyContactInfo = {
			name: "",
			relationship: "",
			number: ""
		};

		/* Declare education background model */
		this.educationInfo = {
			attendedSchool: null,
			stillAttending: null,
			highestLevel: "",
			attendingYearTwelve: null,
			hasVsn: null,
			vsn: "",
			attendedVicSchool: null,
			recentVicShool: "",
			attendedVet: null,
			recentRtos: {
				one: "",
				two: "",
				three: ""
			},
			previousQuals: "",
			previousQualType: "",
			lui: "",
			highSchool: {
				name: "",
				state: "",
				year: ""
			}
		};

		/* Declare employment history model */
		this.employmentInfo = {
			employmentStatus: "",
			occupation: "",
			industry: ""
		}

		/* Declare other details model */
		this.otherInfo = {
			language: "",
			englishLevel: "",
			studyReason: "",
			otherStudyReason: "",
			isDisabled: null,
			disabilityType: "",
			otherDisabilityType: "",
			hasDisabilityPlan: "",
			disabilityManagementPlan: "",
			hasUsi: null,
			usi: "",
			rpl: null,
			ct: null
		}

		/* PTR */
		this.ptr = {
			partA: {
				questionOne: {
					a: "",
					b: "",
					c: ""
				},
				questionTwo: "",
				questionThree: "",
				questionFour: "",
				questionFive: ""
			},
			partB: {
				questionSixA: {
					a: "",
					b: "",
					c: ""
				},
				questionSixB: "",
				questionSeven: {
					a: "",
					b: "",
					c: ""
				}
			},
			partC: {
				questionEight: {
					a: "",
					b: ""
				},
				questionNine: "",
				questionTen: ""
			},
			partD: {
				questionEleven: "",
				questionTwelve: "",
				questionThirteen: {
					a: "",
					b: ""
				}
			}

		}

		/* Applicant Course details */
		this.courseInfo = {
			courseId: "",
			intakeId: ""
		}

		/* selected courses */
		this.courseChoices = {};
		this.declared;

		/* selected course */
		this.selectedCourse;

		/* Array of files 
		this.files = [];
		*/

		/* Current form step */
		this.activeForm = 0;
		/* Academy */
		this.academy;

	}

	/**
	/* Method takes applicant's infor and sends it to API backend */
	Applicant.prototype.getInfo = function () {

		var appInfo = {
			personal: this.personalInfo,
			contact: this.contactInfo,
			emergency: this.emergencyContactInfo,
			educational: this.educationInfo,
			employment: this.employmentInfo,
			other: this.otherInfo,
			courseChoices: this.courseChoices,
			citizenship: this.citizenship,
			funding: this.funding,
			declared: this.declared,
			ptr: this.ptr
		};

		return appInfo;

	}

	/**
	/* Method returns applicant's info for saving on API */
	Applicant.prototype.getSaveInfo = function () {
		var saveInfo = {
			personalInfo: this.personalInfo,
			/* Citizenship status */
			citizenship: this.citizenship,
			/* Course Funding */
			funding: this.funding,
			/* Declare Applicant's contact information model */
			contactInfo: this.contactInfo,
			/* Declare emergency contact model */
			emergencyContactInfo: this.emergencyContactInfo,
			/* Declare education background model */
			educationInfo: this.educationInfo,
			/* Declare employment history model */
			employmentInfo: this.employmentInfo,
			/* Declare other details model */
			otherInfo: this.otherInfo,
			/* Applicant Course details */
			courseInfo: this.courseInfo,
			/* selected courses */
			courseChoices: this.courseChoices,
			declared: this.declared,
			/* selected course */
			selectedCourse: this.selectedCourse,
			/* Array of files */
			ptr: this.ptr
		}

		return saveInfo;
	}

	/**
	 * Method returns true if number of course choices has been reached
	 */
	Applicant.prototype.courseLimitReached = function () {
		return this.courseChoices.length === 1;
	}

	/**
	 * Method returns length of course choices array 
	 */
	Applicant.prototype.getChoicesLength = function () {
		return this.courseChoices.length;
	}

	/**
	 * Method checks if user has selected at least one course 
	 */
	Applicant.prototype.hasChoices = function () {
		return !(this.courseChoices.length <= 0)
	}

	/**
	 * Method deletes object instance 
	 */

	return Applicant;


}]);