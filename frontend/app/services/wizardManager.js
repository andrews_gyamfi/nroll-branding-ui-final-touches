/**
 * TokenFactory is responsible for managing tokens received from backend API.
 * It's responsible for: 
 * 1. Creating tokens on user register and login states 
 * 2. Deleting tokens on user logout 
 */
angular.module("nroll")

/* Requires $window object to store token in localStorage */
.service("wizardManager", [function () {

	/* Enrolment wizard steps */
	this.steps = {
		courseSelection: false,
		personalDetails: false,
		uploads: false,
		declaration: false
	}


	this.setStepValidity = function (validity) {

	}

  }]);