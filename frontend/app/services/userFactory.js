/**
 * UserFactory is responsible for creating new users
 */
angular.module("nroll")

/* Requires: $http, $q and TokenFactory services to function */
.factory("UserFactory", ["$rootScope", "$http", "$q", "TokenFactory", "User", "AUTH_EVENTS", function ($rootScope, $http, $q, TokenFactory, User, AUTH_EVENTS) {
	/**
	 * Method creates new user 
	 * Takes user info input and attempts to create user object 
	 */
	var register = function (userInfo) {
		/* Return if object fields are missing */
		if (!userInfo.firstName || !userInfo.lastName || !userInfo.email || !userInfo.password) {
			$log.error("User object provided is missing either firstName, lastName, email or password fields");
			return;
		}

		return $http.post("/api/auth/register", userInfo)
			.success(function (response) {
				/* Assign token and user object to variables */
				var token = response.token,
					user = response.user;

				/* Store token in local storage and set user login status */
				if (token) {
					TokenFactory.setToken(token);
					User.setLoginStatus(true);
				}

				if (user) {
					/* Assign required value to userInfo object */
					var userInfo = {
						firstName: user.firstName,
						lastName: user.lastName,
						email: user.email
					}

					/* Instantiate user object */
					User.setUser(userInfo);

					/* Broadcast event for newly created user */
					$rootScope.$broadcast(AUTH_EVENTS.USER_CREATED);
				}

			});
	};

	return {
		register: register
	};
}]);