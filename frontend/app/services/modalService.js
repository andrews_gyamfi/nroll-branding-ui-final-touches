/**
 * Modal service manages modals 
 */
angular.module("nroll")

/* Requires: $http and $q objects to function */
.factory("ModalService", ["SweetAlert", "$state", "$rootScope", "$localStorage", function (SweetAlert, $state, $rootScope, $localStorage) {
	var modalService = {};


	modalService.quitNewApplication = function (newState) {
		if (!newState || newState === "") {
			return;
		}
		SweetAlert.swal({
				title: "Are you sure?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, cancel application",
				closeOnConfirm: true
			},
			function (isConfirmed) {
				if (isConfirmed) {
					var appId = $rootScope.currentUser.firstName + "CurrentApp";
					$state.go(newState, {
						reload: true
					});
					delete $localStorage[appId];
				} else {
					$rootScope.applicationInProgress = true;
					return;
				}
			});

	}

	modalService.successfulSubmission = function () {
		$rootScope.applicationInProgress = false;
		SweetAlert.swal({
			title: "Good job!",
			type: "success",
			text: "Your application has been successfully submitted. Please check your email and sign your application.",
			showCancelButton: false,
			confirmButtonColor: "#0c73ba",
			confirmButtonText: "Go to My Applications",
			closeOnConfirm: true
		}, function (isConfirmed) {
			if (isConfirmed) {
				var appId = $rootScope.currentUser.firstName + "CurrentApp";
				$state.go("app.home", {
					reload: true
				});
				delete $localStorage[appId];
			}
		});

	}
	modalService.errorSubmission = function () {
		SweetAlert.swal({
			title: "Oh boy!",
			text: "Please make sure you've completed all *required fields in each section correctly.",
			type: "error",
			showCancelButton: false,
			confirmButtonText: "Yup, will fix it",
			closeOnConfirm: true
		})
	}
	return modalService;
}])