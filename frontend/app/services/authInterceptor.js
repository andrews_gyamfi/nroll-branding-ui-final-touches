/**
 * AuthInterceptor intercepts all requests and 
 * attempts to inject token for API authentication.
 */
angular.module("nroll")

/* Requires: $http, $q and TokenFactory services to function */
.factory("AuthInterceptor", ["$q", "TokenFactory", "$location", function ($q, TokenFactory, $location) {

    /**
     * Method intercepts HTTP request and injects token 
     */
    var request = function (config) {
        var token = TokenFactory.getToken();
        /*config.headers["Authorization"] = "Bearer " + token;*/
        config.headers["X-Access-Token"] = "Bearer " + token;
        return config;
    }

    /**
     * Method intercepts response errors and redirects to login screen
     */
    var responseError = function (response) {
        /*
        if (response.status === 401 || response.status === 403) {
            $location.path("/login");
        }
         Return promise object if errorr */
        return $q.reject(response);
    }


    /* Expose request & response methods via module-revealing pattern */
    return {
        request: request,
        responseError: responseError
    }
}])