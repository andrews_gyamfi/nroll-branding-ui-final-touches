/**
 * AuthFactory is responsible for managing user authentication
 * It's responsible for: 
 * 1. User login
 * 2. User logout
 * 3. Getting user basic information
 */
angular.module("nroll")

/* Requires: $http, $q and TokenFactory services to function */
.factory("AuthFactory", ["$rootScope", "$auth", "$http", "$q", function ($rootScope, $auth, $http, $q) {


    /**
     * Method request basic user info from API
     * User MUST BE LOGGED IN
     */

    var getUser = function () {

        var deferred = $q.defer();

        $rootScope.currentUser = {};
        $rootScope.authenticated = false;

        if ($auth.getToken()) {

            $http.get("api/user/me")
                .then(function (response) {

                    var user = response.data.user;


                    $rootScope.currentUser = user;


                    $rootScope.authenticated = true;

                    deferred.resolve(response);
                })
                .catch(function (response) {
                    deferred.reject();
                })
        } else {
            $rootScope.currentUser.firstName = "";
        }

        return deferred.promise;
    }


    /* Expose login, logout, getUser & isLoggedIn methods via module-revealing pattern */
    return {
        getUser: getUser
    }
}])