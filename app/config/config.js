/**
 * Main application configuration file
 * Overall configuration will depend on which mode the app is running.
 * The default run mode is 'development' 
 */

// Lodash will be used for merging main configuration with running mode configuration file
// So for example, if the 'mode' property is set to development, then we will grab the 'development.js' file
// from the configuration folder and merge it with this configuration file.
var _ = require("lodash");

var config = {
	dev: "development",
	test: "testing",
	prod: "production",
	port: process.env.PORT || 3000,
	expireTime: 24 * 60 * 10,
	secrets: {
		jwt: process.env.JWT || 'h@rd3Stp@ssw0rDh@$#p@$#3vAd0n0Tr1th13@Home'
	},
	mailgun: {
		user: process.env.MAIL_GUN_USER || "postmaster@sandboxac29935df1b146c7a734e3be23ba9793.mailgun.org",
		pass: process.env.MAIL_GUN_PASS || "29d1d599a1fdb5fb782077fcbde2403d"
	},
	smtp: {
		user: "apply@pt.edu.au",
		password: "P@ssw0rd@2016",
		host: "smtp.office365.com",
		port: "587"
	},
	salesforce: {
		username: process.env.SF_USER || "apply@pt.edu.au",
		password: process.SF_PASS || "p@ssw0rD1$",
		accessToken: "GXupbEJPzZp2lBnGBVhnaSVlm"
	},
	mongo: {
		db: "enroll",
		url: "mongodb://localhost/nroll" || "mongodb://nroll:passw0rD@ds021434.mlab.com:21434/nroll"
	}
};


module.exports = config;