/**
 * Load up all required middlewares
 */
var bodyParser = require("body-parser"),
    morgan = require("morgan"),
    passport = require("passport"),
    config = require("../config/config"),
    User = require("../api/user/userModel"),
    jwt = require("jsonwebtoken");

module.exports = function (app) {

    app.use(morgan('dev'));

    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(bodyParser.json());

    require("../services/passportAuth")(app);
}