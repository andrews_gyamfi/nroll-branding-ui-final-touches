var _ = require("lodash"),
    fs = require("fs"),
    jwt = require("jsonwebtoken"),
    user = require("../api/user/userModel"),
    config = require("../config/config"),
    path = require("path");



/**
 * Load template settings for lodash for interpolation 
 */
_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};


/**
 * Function reads email template file and interpolates respective data fields
 * @param token token to be inserted into link button in email
 * @returns template file 
 */

exports.buildEmail = function getHtml(token, templatePath, model) {

    /* Return if required parameters are not provided */
    if (!token || !templatePath) {
        return;
    }

    /* Normalize path */
    var filePath = path.normalize(__dirname + templatePath);

    /* Try loading template file */
    try {

        var html = fs.readFileSync(filePath, "utf8");

    } catch (e) {
        throw "Could not fetch template file";
    }

    var template = _.template(html);

    return template(model);

}