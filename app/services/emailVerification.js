var _ = require("lodash"),
	fs = require("fs"),
	jwt = require("jsonwebtoken"),
	nodemailer = require("nodemailer"),
	user = require("../api/user/userModel"),
	smtpTransport = require("nodemailer-smtp-transport"),
	config = require("../config/config"),
	path = require("path"),
	templateBuilder = require("./emailTemplateBuilder");

/**
 * Method sends emeail
 * @param email address of receipient 
 */
exports.send = function (email, token, subject, htmlTemplatePath, model) {
	var emailTemplate = templateBuilder.buildEmail(token, htmlTemplatePath, model);

	var transporter = nodemailer.createTransport(smtpTransport({
		host: config.smtp.host,
		port: config.smtp.port,
		secureConnection: 'false',
		tls: {
			ciphers: 'SSLv3'
		},
		auth: {
			user: config.smtp.user,
			pass: config.smtp.password
		}
	}));


	var mailOptions = {
		from: config.smtp.user,
		to: email,
		subject: subject,
		html: emailTemplate
	};

	transporter.sendMail(mailOptions, function (err, info) {
		if (err) {
			return false;
			console.log(err);
		}

		console.log(info);
	})


	return true;
}