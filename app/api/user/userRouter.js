var router = require("express").Router(),
    controller = require("./userController"),
    User = require("./userModel"),
    passport = require("passport"),
    jwt = require("jsonwebtoken"),
    config = require("../../config/config"),
    EmailManager = require("../../services/emailVerification"),
    LocalStrategy = require("passport-local");

router.param("id", controller.params);

router.get("/", function (req, res, next) {
    res.json({
        success: true,
        message: "You made it!"
    })
})

router.get("/me", function (req, res, next) {
    if (!req.user) {
        res.status(401).send("You are not authorized");
    }
    res.json({
        user: req.user
    });
});

router.get("/:id", controller.getOne);

router.post("/reset", controller.resetPassword);


module.exports = router;