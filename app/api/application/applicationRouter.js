var router = require("express").Router(),
	validator = require("express-validator"),
	customValidator = require("../../util/validator"),
	controller = require("./applicationController");

/* Validate Input 
router.use(customValidator);*/

router.use(validator());

router.param("id", controller.params);

router.get("/myapplications", controller.getMyApplications);

router.post("/save", controller.saveIncompleteApplication);

router.get("/:id", controller.getOne);

router.route("/")
	.get(controller.getAll)
	.post(customValidator, controller.create);


module.exports = router;