/**
 * Controller to handle all things relating to the application object.
 * This will also be used in the router
 */
var Application = require("./applicationModel"),
	Salesforce = require("../../services/salesforce")(),
	User = require("../user/userModel");


/**
 * Method grabs 'id' parameter value, fetches the relevant object and attaches it to the req object for
 * use by other middleware and routing request */

exports.params = function (req, res, next, id) {
	Application.findById(id)
		.then(function (application) {
			req.application = application;
			next();
		})
		.catch(function (err) {
			next(err);
		});
}


/**
 * Method creates a new object into the database and returns the object
 */
exports.create = function (req, res, next) {

	/* Get reference to posted data */
	var d = req.body;

	/* Method checks age based on input date*/
	function calculateAge(birthday) { // birthday is a date
		var ageDifMs = Date.now() - birthday.getTime();
		var ageDate = new Date(ageDifMs); // miliseconds from epoch
		return Math.abs(ageDate.getUTCFullYear() - 1970);
	}

	/* Sanitize input before proceeding */
	req.sanitizeBody();


	/* If all good with data, proceed with inserting info to salesforce */
	if (d) {

		/* Attempt inserting into salesforce */
		Salesforce.createNewApp({
			Able_to_Study_in_Australia__c: Boolean(d.citizenship.otherVisa.allowedToStudy) ? "Yes" : "No",
			Acknowledgement__c: Boolean(d.declared) ? "Yes" : "No",
			Applying_for_Credit_Transfer__c: Boolean(d.other.ct) ? "Yes" : "No",
			Applying_for_RPL__c: Boolean(d.other.rpl) ? "Yes" : "No",
			/*Attend_School__c,*/
			Attending_Year_Twelve__c: Boolean(d.educational.attendingYearTwelve) ? "Yes" : "No",
			Campus__c: d.courseChoices.campus.id,
			CHESSN__c: d.funding.vfh.CHESSN,
			Citizenship__c: d.citizenship.country,
			Country_of_Birth__c: d.personal.countryOfBirth,
			Course__c: d.courseChoices.course.id,
			Cultural_Background__c: d.personal.culturalBackground,
			Date_Of_Birth__c: new Date(d.personal.dateOfBirth),
			Disability_Management_Plan__c: Boolean(d.other.hasDisabilityPlan) ? "Yes" : "No",
			disability_managment__c: d.other.disabilityManagementPlan,
			Disability_Type__c: d.other.disabilityType,
			Emergency_Relationship__c: d.emergency.relationship,
			Emergency_Contact__c: d.emergency.name,
			Emergency_Contact_Number__c: d.emergency.number,
			Employment_Status__c: d.employment.employmentStatus,
			Language_Level__c: d.other.englishLevel,
			First_Name__c: d.personal.firstName,
			Gender__c: d.personal.gender,
			Has_Disibiity__c: Boolean(d.other.isDisabled) ? "Yes" : "No",
			/*Has_Other_Visa__c*/
			Has_VSN__c: Boolean(d.educational.hasVsn) ? "Yes" : "No",
			Have_accessed_VET_FEE_HELP_previously__c: Boolean(d.funding.vfh.previouslyAccessedVfh) ? "Yes" : "No",
			Highest_Level_of_Schooling__c: d.educational.highestLevel,
			Highest_Previous_Qualifications__c: d.educational.previousQuals,
			Highest_Previous_Qualification_Type__c: d.educational.previousQualType,
			Intake__c: d.courseChoices.intake.id,
			Last_Name__c: d.personal.lastName,
			LUI___c: d.educational.lui,
			Most_Recent_Provider_Name__c: d.funding.vfh.recentProvider.name,
			New_SCV__c: Boolean(d.citizenship.scvVisa) ? "Yes" : "No",
			Occupation__c: d.employment.occupation,
			Occupation_Industry__c: d.employment.industry,
			Other_Disability__c: d.other.otherDisabilityType,
			other_study_reason__c: d.other.otherStudyReason,
			Parent_or_Guardian_Email__c: d.contact.parentEmail,
			Participated_in_HEP_or_RTO_education__c: Boolean(d.funding.vfh.hasRecentProvider) ? "Yes" : "No",
			Pos_Postcode__c: d.contact.address.postal.postcode,
			Pos_State__c: d.contact.address.postal.state,
			Pos_Suburb__c: d.contact.address.postal.suburb,
			Postal_Address__c: d.contact.address.postal.street,
			Previous_First_Name__c: d.personal.previousFirstName,
			Previous_Last_Name__c: d.personal.previousLastName,
			Previous_Name__c: Boolean(d.personal.hasPreviousName) ? "Yes" : "No",
			Primary_Contact_Number__c: d.contact.mobile,
			Primary_Email__c: d.contact.email,
			Question_One_A__c: d.ptr.partA.questionOne.a,
			Question_One_B__c: d.ptr.partA.questionOne.b,
			Question_One_C__c: d.ptr.partA.questionOne.c,
			Question_Two__c: d.ptr.partA.questionTwo,
			Question_Three__c: d.ptr.partA.questionThree,
			Question_Four__c: d.ptr.partA.questionFour,
			Question_Five__c: d.ptr.partA.questionFive,
			Question_Six_A_One__c: d.ptr.partB.questionSixA.a,
			Question_Six_A_Two__c: d.ptr.partB.questionSixA.b,
			Question_Six_A_Three__c: d.ptr.partB.questionSixA.c,
			Question_Six_B__c: d.ptr.partB.questionSixB,
			Question_Seven_A__c: d.ptr.partB.questionSeven.a,
			Question_Seven_B__c: d.ptr.partB.questionSeven.b,
			Question_Seven_C__c: d.ptr.partB.questionSeven.c,
			Question_Eight_A__c: d.ptr.partC.questionEight.a,
			Question_Eight_B__c: d.ptr.partC.questionEight.b,
			Question_Nine__c: d.ptr.partC.questionNine,
			Question_Ten__c: d.ptr.partC.questionTen,
			Question_Eleven__c: d.ptr.partD.questionEleven,
			Question_Twelve__c: d.ptr.partD.questionTwelve,
			Question_Thirteen_A__c: d.ptr.partD.questionThirteen.a,
			Question_Thirteen_B__c: d.ptr.partD.questionThirteen.b,
			Recent_RTO_School_Name_One__c: d.educational.recentRtos.one,
			Recent_RTO_School_Name_Two__c: d.educational.recentRtos.two,
			Recent_RTO_School_Name_Three__c: d.educational.recentRtos.three,
			/*Recent_Victorian_School__c: "",*/
			Related_Lead__c: req.user.sfId,
			/*Related_Hire_Agreement__c,*/
			Res_Postcode__c: d.contact.address.residential.postcode,
			Res_State__c: d.contact.address.residential.state,
			Res_Suburb__c: d.contact.address.residential.suburb,
			Res_Street_Address__c: d.contact.address.residential.street,
			School_Name__c: d.educational.highSchool.name,
			School_State__c: d.educational.highSchool.state,
			/*Secondary_Contact_Number__c,*/
			Spoken_Language__c: d.other.language,
			Still_Attending_School__c: Boolean(d.educational.stillAttending) ? "Yes" : "No",
			Student_Number__c: d.funding.vfh.recentProvider.studentNumber,
			Study_Reason__c: d.other.studyReason,
			Title__c: d.personal.title,
			Under_18__c: calculateAge(new Date(d.personal.dateOfBirth)) < 18 ? "Yes" : "No",
			usi__c: d.other.usi,
			VET_FEE_Help_Intent__c: Boolean(d.funding.vfh.wantsToAccess) ? "Yes" : "No",
			/*Visa_Expiry_Date__c: d.citizenship.otherVisa.expiryDate,*/
			Visa_Subcategory__c: d.citizenship.otherVisa.subCategory,
			Visa_Type__c: d.citizenship.visaStatus,
			VSN__c: d.educational.vsn,
			Year_last_attended_school__c: d.educational.highSchool.year,
			Year_Last_Enrolled__c: d.funding.vfh.recentProvider.yearLastEnrolled,
			Year_of_Arrival__c: d.personal.yearOfArrival

		}, function (id) {
			/* If no id was returned, insertion was not successful. Return error */
			if (!id) {
				returnError()
			}

			/* If id provided, insertion was successful, lookup authenticated user and add application metadata to array of applications */
			if (req.user) {

				/* Authenticated user handle */
				var currentUser = req.user;

				/* Create an application metadata object - REMEMBER we not store completed applications in our DB */
				var applicationMetaInfo = {
					courseCode: d.courseChoices.course.name,
					courseName: d.courseChoices.course.code,
					status: "Completed",
					sfId: id
				};

				/* Attempt to find current user by email */
				User.findOne({
					email: currentUser.email
				}, function (err, user) {
					/* Something went wrong and user could not be found. Return error */
					if (err) {
						returnError()
					}

					user.applications.push(applicationMetaInfo);

					user.save()
						.then(function (user) {
							console.log(user);
							return res.json({
								id: id,
								succces: true
							});
						})
						.catch(function (err) {
							console.log(err);
							return returnError()
						})
				})

			}

			/* Method returns error on task failure */
			var returnError = function () {
				return res.status(400).send({
					succces: false
				});
			}

		})

	}



}

/**
 * Method finds object by Id and returns it 
 * Simple, it returns the req.application object fetched by the params middleware - above
 */
exports.getOne = function (req, res, next) {

	var appId = req.params.id;

	if (!req.user) {
		return res.status(401).send();
	}

	if (!appId) {
		return res.status(400).send();
	}

	Application.find({
			_id: appId
		})
		.then(function (application) {
			console.log(application);
			return res.status(200).send({
				success: true,
				application: application
			});
		})
		.catch(function (err) {
			return res.status(400).send({
				success: false
			});
		})
}

/**
 * Method returns all applicaion objects
 */
exports.getAll = function (req, res, next) {
	return Application.find({})
		.then(function (data) {
			res.json(data || {});
		})
		.catch(function (err) {
			next(err);
		})
}

/**
 * Method exports applications
 */
exports.getMyApplications = function (req, res, next) {

	if (req.user) {
		User.findOne({
				email: req.user.email
			})
			.then(function (user) {
				if (!user) {
					return res.status(400).send({
						success: false,
						applications: []
					});
				}
				applications = user.getMyApplications();

				res.status(200).send({
					succes: true,
					applications: applications
				});
			})
			.catch(function (err) {

			})

	}

}

exports.getIncompleteApplications = function (req, res, next) {
	if (req.user) {
		User.findOne({
				email: req.user.email
			})
			.populate()
	}
}

/** 
 * 
 */
exports.saveIncompleteApplication = function (req, res, next) {
	var formInfo = req.body;

	console.log(formInfo);

	if (!formInfo.courseChoices) {
		console.log("course choices error");
		return res.status(400).send({
			succces: false
		})
	}

	var temp = new Application(formInfo);

	console.log(temp);


	temp.save()
		.then(function (temp) {})
		.catch(function (err) {
			console.log(err);
			console.log("THERE WAS AN ERROR TRYINT TO SAVE THIS DOCUMENT");
			return res.status(400).send({
				succces: false
			})
		})

	if (temp) {
		User.findOne({
				email: req.user.email
			})
			.then(function (user) {
				user.applications.push({
					courseCode: formInfo.courseChoices.course.name,
					courseName: formInfo.courseChoices.course.code,
					status: "Saved",
					appRefId: temp._id
				});

				user.save(function (err) {
					console.log(err);
					return res.status(200).send({
						success: true
					});
				});
			})
			.catch(function (err) {
				return res.status(400).send();
			})


	}

}