/**
 * AuthRouther handles all things relating to registration, login and email verification
 */

/* Declare variables */
var router = require("express").Router(),
	User = require("../user/userModel"),
	config = require("../../config/config"),
	jwt = require("jsonwebtoken"),
	passport = require("passport"),
	EmailManager = require("../../services/emailVerification"),
	LocalStrategy = require("passport-local"),
	validator = require("express-validator"),
	path = require("path");

/* Use expresss validator on route */
router.use(validator());

/**
 * Registration handler
 * 1. Checks if user already exists, return error message 
 * 2. Otherwise it attempts to create user object in database 
 * 3. The email managere sends an email to the newly created user to verify email address 
 */
router.post("/register", function (req, res, next) {

	/* Reference to form input data */
	var inputData = req.body;

	/* Validation errors reference */
	var validationErrors;

	/* Validation Schema */
	var validationSchema = {
		'email': {
			notEmpty: true,
			isEmail: {
				errorMessage: "Invalid Email"
			}
		},
		'firstName': {
			notEmpty: true,
			isAlpha: {
				errorMessage: "Invalid First Name."
			}
		},
		'lastName': {
			notEmpty: true,
			isAlpha: {
				errorMessage: "Invalid Last Name."
			}
		},
		'password': {
			notEmpty: true,
			isLength: {
				options: [{
					min: 6,
					max: 30
				}],
				errorMessage: "Password must be between 6 and 30 characters long."
			}
		},
		'cfPassword': {
			notEmpty: true,
			isLength: {
				options: [{
					min: 6,
					max: 15
				}],
				equals: req.body.password,
				errorMessage: "Passwords must match."
			}
		},
		"consultationId": {
			notEmpty: true,
			errorMessage: "Invalid Consultation ID."
		}
	}

	/* If no data is provided, return */
	if (!inputData) {
		return res.status(400).send({
			success: false,
			message: "Required information missing."
		});
	}

	/* Sanitize input before proceeding */
	req.sanitizeBody();

	/* Validate input */
	req.checkBody(validationSchema);

	/* Validation Errors */
	validationErrors = req.validationErrors();

	/* Return validation errors if not valid */
	if (validationErrors) {
		console.log(validationErrors);
		return res.status(400).send({
			success: false,
			message: "Please make sure you've entered all details correctly."
		});
	}


	/* Use passport.authenticate method to attempt login */
	passport.authenticate("local-register", {
		session: false,
	}, function (err, user) {

		/* Return error if error */
		if (err) {
			return res.status(400).send({
				success: false,
				message: "Please make sure you've entered all details correctly."
			});
		}

		/* If user already exists, return error message */
		console.log("FOUND USER - NOW CHECKING RESULTS");
		console.log(user);

		if (user.succces === false) {
			res.status(400).send({
				success: false,
				message: "An account already exists with this email"
			});

		} else {
			/* Otherwise create and send token to app */
			var userToken = createAndSendToken(user.toJSON());

			return res.json({
				success: true,
				user: user,
				token: userToken
			});

			/* Email user to verify email address 
			EmailManager.send(user.email);
			*/
		}

	})(req, res, next);

});

/**
 * Login Handler 
 * 1. Attempts to log in user using local passport strategy
 */

router.post("/login", function (req, res, next) {

	passport.authenticate("local-login", {
		session: true
	}, function (err, user, info) {

		/* Return error if error */
		if (err) {
			return res.status(401).send({
				success: false
			});
		}

		/* Attempt to login */
		req.login(user, function (err) {
			if (err) {
				return res.status(401).send({
					success: false
				});
			}
			/* Create and send token to app */
			var userToken = createAndSendToken(user.toJSON());

			return res.json({
				success: true,
				user: user,
				token: userToken
			});
		})


	})(req, res, next);

});


/**
 * Email verification handler
 * Grabs token off url params and attempt to authenticate it 
 * If valid, sets user's active property to true.
 */

router.get("/verifyEmail", function (req, res, next) {

	/* Get token off query string */
	var token = req.query.token;

	/* Decode token */
	var payload = jwt.verify(token, config.secrets.jwt);

	/* Grab email off decoded token */
	var email = payload.sub;

	/* If email not found, return error message */
	if (!email) console.log("no email available");

	/* If email, attempt to retrieve user object */
	User.findOne({
		email: email
	}, function (err, user) {

		/* Log error if error */
		if (err) console.log(err);

		/* If user is not active, set active to true */
		if (!user.active) {
			user.active = true;
			user.save(function (err) {
				if (err) {
					console.log("Could not save user!");
				} else {
					res.send("Your email account " + email + " " + " has now been verified.");
				}

			})
		} else {
			res.send("Your account has already been verifed");
		}


	})

});



router.get("/resetpassword", function (req, res, next) {

	/* Get token off query string */
	var token;

	/* Decode token */
	var payload;

	if (!req.query.token) {
		return res.status(400)
			.send({
				success: false
			});
	}

	token = req.query.token;
	payload = jwt.verify(token, config.secrets.jwt);

	/* Grab email off decoded token */
	var email = payload.sub;

	/* If email not found, return error message */
	if (!email) {
		return res.status(400)
			.send({
				success: false
			});
	}

	/* If email, attempt to retrieve user object */
	User.findOne({
		email: email
	}, function (err, user) {

		if (user.resetToken !== token) {
			return res.sendFile(path.resolve(__dirname + "../../../../frontend/views/linkExpired.html"));
		}

		/* Log error if error */
		if (err) {
			return res.status(400)
				.send({
					success: false
				});
		}

		return res.sendFile(path.resolve(__dirname + "../../../../frontend/views/resetpassword.html"));
		/* If user is not active, set active to true 
		if (!user.active) {
		    user.active = true;
		    user.save(function (err) {
		        if (err) {
		            console.log("Could not save user!");
		        } else {
		            res.send("Your email account " + email + " " + " has now been verified.");
		        }

		    })
		} else {
		    res.send("Your account has already been verifed");
		}
		*/

	})


});

/** 
 * Method resets user's password 
 */

router.post("/reset", function (req, res, next) {

	/* Get Email address */
	var email = req.body.email || "",
		token = "",
		emailTemplate = "../../views/resetPassword.html",
		emailSubject = "Reset your password",
		model = {
			firstName: "",
			resetPassword: ""
		};

	if (!email) {
		return res.status(400).send({
			succces: false
		});
	}

	/* Find if user is registered */
	User.findOne({
			email: email
		})
		.then(function (user) {
			if (!user) {
				return res.status(400).send({
					succces: false
				});
			}

			token = createEmailToken(user);

			model.firstName = user.firstName;
			model.resetPassword = "https://apply.peg.edu.au/api/auth/resetpassword?token=" + token;

			var sentEmail = EmailManager.send(user.email, token, emailSubject, emailTemplate, model);

			if (!sentEmail) {
				return res.status(400).send({
					succces: false
				});
			}

			user.resetToken = token;

			user.save(function (err, user) {
				if (err) {
					return res.status(400).send({
						succces: false
					});
				}

				return res.status(200).send({
					succces: true
				});
			});


		})
		.catch(function (err) {
			console.log(err);
			return res.status(400).send({
				succces: false
			});
		})
})


/** 
 * Method creates new user password
 */

router.post("/newpassword", function (req, res, next) {
	var newPassword = req.body.newPassword,
		confirmNewPassword = req.body.cNewPassword,
		token = req.body.token;

	var validationErrors = "";

	var validationSchema = {
		'newPassword': {
			notEmpty: true,
			isLength: {
				options: [{
					min: 6,
					max: 15
				}],
				errorMessage: "Password must be between 6 and 15 characters long."
			}
		},
		'cNewPassword': {
			notEmpty: true,
			isLength: {
				options: [{
					min: 6,
					max: 15
				}],
				equals: req.body.newPassword,
				errorMessage: "Passwords must match."
			}
		}
	}

	/* Sanitize input before proceeding */
	req.sanitizeBody();

	/* Validate input */
	req.checkBody(validationSchema);


	/* Validation Errors */
	validationErrors = req.validationErrors();

	if (validationErrors) {
		return res.status(400).send({
			succces: false
		});
	}


	/* Decode token */
	var payload = jwt.verify(token, config.secrets.jwt);

	/* Grab email off decoded token */
	var email = payload.sub;

	/* Find if user is registered */
	User.findOne({
			email: email
		})
		.then(function (user) {
			if (!user) {
				return res.status(400).send({
					succces: false
				});
			}

			if (user.resetToken !== token) {
				return res.status(400).send({
					succces: false,
					message: "Token expired!"
				});
			}

			user.password = user.encryptPassword(newPassword);
			user.resetToken = "";

			user.save(function (err, user) {
				if (err) {
					return res.status(400).send({
						succces: false
					});
				}

				return res.status(200).send({
					succces: true
				});
			});


		})
		.catch(function (err) {
			console.log(err);
			return res.status(400).send({
				succces: false
			});
		})


})

/** 
 * Method creates web tokens based on user id
 */
function createAndSendToken(user, res) {
	/* Token payload */
	var payload = {
		sub: user._id
	};

	/* Create token */
	var token = jwt.sign(payload, config.secrets.jwt);

	return token;
}

/** 
 * Method creates token based on user's email 
 */
function createEmailToken(user) {
	var payload = {
		sub: user.email
	}

	var token = jwt.sign(payload, config.secrets.jwt);

	return token;
}


module.exports = router;