/**
 * Api.js brings all routing functionality into one place for each of the various resources
 */
var router = require("express").Router(),
	applicationRouter = require("./application/applicationRouter"),
	courseRouter = require("./courses/courseRouter"),
	uploadRouter = require("./upload/uploadRouter"),
	authRouter = require("./auth/authRouter"),
	jwt = require("jsonwebtoken"),
	config = require("../config/config"),
	User = require("./user/userModel"),
	userRouter = require("./user/userRouter");




/** 
 * Middleware function to ensure user is authenticated 
 * To be used on secure routes 
 */
var ensureAuth = function (req, res, next) {

	if (!req.headers.authorization) {
		return res.status(401).send("You are not authorized");
	}

	/* Declare variable to store provided token */
	var authHeader = req.headers.authorization;
	/* Grab token */
	var token = authHeader.split(" ")[1];

	/* If no token was provided, return 401 Unauthorized code */


	/* Decode token for payload */
	var payload = jwt.verify(token, config.secrets.jwt);


	if (payload) {

		/* Grab user's id from token */
		var userId = payload.sub;

		/* Find specific user in DB */
		User.findOne({
			_id: userId
		}, function (err, user) {
			/* If error return 401 Unauthorized code */
			if (err) {
				return res.status(401).send("You are not authorized");
			}

			/* If user does not exist, someone may be trying to hack the system */
			if (!user) {
				return res.status(401).send("You are not authorized");
			}

			/* Add user to req object */
			req.user = user;

			/* Move to next middleware */
			next();
		})
	}

}

// Assign routes to respective Router
router.use("/application", ensureAuth, applicationRouter);
router.use("/user", ensureAuth, userRouter);
router.use("/courses", ensureAuth, courseRouter);
router.use("/upload", ensureAuth, uploadRouter);
router.use("/auth", authRouter);

module.exports = router;