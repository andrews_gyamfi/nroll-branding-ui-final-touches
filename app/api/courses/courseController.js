/**
 * Course controller uses Salesforce service 
 * to retrieve course information from salesforce.
 * Collection is passed as JSON object to frontend for processing
 */

var Salesforce = require("../../services/salesforce")(),
    _ = require("lodash");

/**
 * Method returns all ACTIVE courses from salesforce
 */
exports.getAllIntakes = function (req, res, next) {

    Salesforce.getAllIntakes(function (data) {

        var sanitizedData = [];

        if (data) {
            _.forEach(data, function (intake) {
                delete intake.Campus__r.attributes;
                delete intake.Course__r.attributes;
                delete intake.attributes;
                sanitizedData.push(intake);
            })

            return res.send({
                success: true,
                data: sanitizedData
            })
        } else {
            return res.send({
                success: false,
                message: "Could not get course info"
            });
        }
    })
}