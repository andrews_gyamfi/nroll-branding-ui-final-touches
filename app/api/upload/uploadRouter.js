/**
 * Route is responsible for file uploads
 */

/* Require necessary files */
var route = require("express").Router(),
	fs = require("fs"), //node file stream
	_ = require("lodash"), //data manipulation library
	salesforce = require("../../services/salesforce")(), //custom salesforce service
	jimp = require("jimp"), //image manipulation library - for image compression
	path = require("path"),
	multer = require("multer"); //file upload library 

/* Set up multer disk storage */
var storage = multer.diskStorage({
	/* File destination */
	destination: function (req, file, cb) {
		cb(null, './')
	},
	/* File name */
	filename: function (req, file, cb) {
		var datetimestamp = Date.now();
		cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
	}
});

/* Get instance of multer - single uploads */
var upload = multer({
	storage: storage
}).any([
    "document0",
    "document1",
    "document2",
    "document3",
    "document4",
    "document5",
    "document6",
    "document7",
    "document8",
    "document9",
    "document10"
]);



/* Post route handelr */
route.post("/:id", function (req, res, next) {

	var uploadFailed = true;
	var userId = req.params.id;

	/* Attempt upload of file on local server */
	upload(req, res, function (err) {

		if (err) {
			/* return error message if error */
			res.send("Something went wrong. Could not upload file.");

			/* Output error on server console */
			//            console.log("ERROR CODE: " + err.error_code + " | ERROR_DESC: " + err.err_desc);
			console.log("THERE WAS AN ERROR");
			return;
		}

		if (req.files || req.file) {
			_.forEach(req.files, function (loopFile) {

				/* If file has been uploaded on server, grab details and attempt upload to Salesforce */
				var filepath = loopFile.path,
					fileName = loopFile.filename,
					fileType = loopFile.mimetype;

				/* Attempt to read file */
				fs.readFile(filepath, function (err, file) {

					if (err) {

						/* Return error message if error */
						res.send("Something went wrong. Could not upload file.");

						/* Log erorr if any */
						console.log("Could not fetch file");

					} else {
						/* Convert file to base64 */
						var base64data = new Buffer(file).toString("base64");

						/* Attempt to upload file to Salesforce. Callback function is invoked if attachment was successful */
						salesforce.addAttachment(fileName, base64data, fileType, userId, function (fileName) {
							if (!fileName) {
								uploadFailed = true;
								return;
							}

							/* Delete file from local server after upload */
							fs.unlink(fileName, function (err) {
								if (err) {
									/* Log error on console */
									console.log(err);

								} else {

								}
							})
						});
					}
				})

			})

			res.send({
				success: true
			});

		}

	});

});

/* Get router handler */
route.get("/", function (req, res, next) {
	res.render("../views/upload.jade");
})

/* Expose module */
module.exports = route;