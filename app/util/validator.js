/**
 * Handles input validation
 */
var validate = require("express-validator"),
	_ = require("lodash");

/**
 * Method handles form validation
 */
var Validator = function (req, res, next) {

	/* Declare arrays to hold different input types */
	var rawData = req.body;

	/* Method parses dotted string and access relevant value from post object */
	var stringToParam = function (stringParam) {
		if (!stringParam || stringParam === 0) {
			return
		}

		var strArray = stringParam.split("."),
			objValue = "",
			i;
		for (i = 0; i < strArray.length; i++) {
			if (!req.body[strArray[i]]) {
				return null;
			}
			objValue = objValue[strArray[i]];
		}

		return objValue;
	}

	/* Method nullifies object */
	var nullify = function (param) {
		var i = 0;
		if (Array.isArray(param)) {
			for (i = 0; i < param.length; i++) {
				param[i] = null;
			}
		}

		param = null;
	}

	/* Function trims and convert string to lowercase */
	var cleanStr = function (str) {
		if (str) {
			return str.toLowerCase().trim();
		}
	}


	inputTypes = {
		onlyStringInput: [],
		onlyNumericInput: [],
		onlyDateInput: [],
		alphaNumericInput: [],
		booleanInput: [],
		invalidInput: []
	};

	/* Array of all string inputs */
	inputTypes.onlyStringInput = {
		noSpacesAllowed: [
            "personal.title",
            "personal.firstName",
            "personal.lastName",
            "personal.previousFirstName",
            "personal.previousLastName",
            "personal.gender",
            "contact.address.residential.state",
            "contact.address.postal.suburb",
            "contact.address.postal.state",
            "emergency.relationship",
            "educational.highSchool.state"
        ],
		spacesAllowed: [
            "educational.highestLevel",
            "contact.address.residential.suburb",
            "contact.address.residential.suburb",
            "contact.address.residential.street",
            "contact.address.postal.street",
            "personal.culturalBackground",
            "emergency.name",
            "personal.countryOfBirth",
            "citizenship.country",
            "citizenship.visaStatus",
            "citizenship.otherVisa.subCategory",
            "educational.highSchool.name",
            "educational.previousQuals",
            "employment.employmentStatus",
            "employment.occupation",
            "employment.industry",
            "other.language",
            "other.englishLevel",
            "other.disabilityType",
            "other.studyReason",
            "other.otherStudyReason",
            "educational.recentVicShool",
            "funding.vfh.recentProvider.name"
        ]

	};

	/* Array of numeric inputs e.g. postal code, phone numbers, etc. */
	inputTypes.onlyNumericInput = [
            "educational.vsn",
            "contact.address.residential.postcode",
            "contact.address.postal.postcode",
            "contact.mobile",
            "emergency.number",
            "educational.highSchool.lui"
    ];

	/* Array of date inputs e.g. date of birth, year of arrival, etc. */
	inputTypes.onlyDateInput = [
        "personal.dateOfBirth",
        "personal.yearOfArrival",
        "educational.highSchool.year",
        "citizenship.otherVisa.expiryDate",
        "funding.vfh.recentProvider.yearLastEnrolled"
    ]

	/* Array of alpha numeric inputs e.g. street address */
	inputTypes.alphaNumericInput = [
        "rawData.other.usi",
        "funding.vfh.recentProvider.studentNumber",
        "funding.vfh.CHESSN"
    ];

	/* Array of boolean inputs */
	inputTypes.booleanInput = [
        "personal.hasPreviousName",
        "contact.address.same",
        "funding.vfh.wantsToAccess",
        "funding.vfh.hasRecentProvider",
        "funding.vfh.previouslyAccessedVfh",
        "educational.attendedSchool",
        "educational.stillAttending",
        "educational.attendingYearTwelve",
        "educational.attendedVicSchool",
        "educational.attendedVet",
        "educational.hasVsn",
        "other.hasUsi",
        "other.isDisabled",
        "other.disabilityType",
        "other.otherDisabilityType",
        "other.hasDisabilityPlan",
        "other.disabilityManagementPlan",
        "citizenship.otherVisa.allowedToStudy",
        "citizenship.scvVisa"
    ]

	var validateBoolean = function () {
		req.checkBody(inputTypes.booleanInput).isBoolean();
	}

	/* Method validates personal info 
	var validatePersonalInfo = function () {

	    _.forEach(rawData.personal, function (value, key) {
	        if (key === "gender") {
	            req.checkBody("personal." + key, "Please select the right gender").isAlpha.isLength({
	                min: 1,
	                max: 6
	            });
	        }

	        if (key === "title" || key === "firstName" || key === "lastName" || key === "previousFirstName" || key === "previousLastName") {
	            req.checkBody("personal." + key, "At least 2 characters required").isAlpha().isLength({
	                min: 2,
	                max: 60
	            });
	        }

	        if (key === "countryOfBirth") {
	            req.checkBody("personal." + key, "Wrong input for country").isAscii().isLength({
	                min: 4,
	                max: 100
	            });
	        }

	        if (key === "countryOfBirth" && value !== "Australia") {
	            req.checkBody("personal." + 'culturalBackground').optional();

	        } else {
	            req.checkBody("personal." + 'yearOfArrival').optional();
	        }

	        if (key === "dateOfBirth") {
	            req.checkBody("personal." + key, "Please enter a valid date").isDate();
	        }

	        if (key === "hasPreviousName") {

	        }
	    })
	}
	*/

	/**
	 * Method loops through all inputs where spaces IS NOT ALLOWED to make sure this data is as expected
	 * 1. Names are expected to have a minimum length of 2 and maximum length of 60
	 */

	var validateNoSpaces = function () {
		_.forEach(inputTypes.onlyStringInput.noSpacesAllowed, function (value) {
			if (stringToParam(value) === null) {
				console.log("ignored: " + value);
				return;
			}


			if (req.checkBody(value, "Please provide the correct input").isAlpha()) {
				req.checkBody(value, value + " " + "At least 2 characters required").isLength({
					min: 2,
					max: 100
				});
			}
		});
	}


	/**
	 * Method loops through all inputs where spaces IS ALLOWED to make sure this has not been completed by a bot
	 */
	var validateSpacesAllowed = function () {
		_.forEach(inputTypes.onlyStringInput.spacesAllowed, function (value) {
			if (stringToParam(value) === null) {
				console.log("ignored: " + value);
				return;
			}
			req.checkBody(value, "Invalid String").isAscii().isLength({
				min: 2,
				max: 255
			});
		});
	}

	/**
	 * Method loops through all numeric inputs and validate as necessary
	 * 1. Post codes should have a minimum length of 4 and max of 5 and should be only numeric characters
	 * 2. Contact numbers are validated based on Australian requirements
	 
	var validateNumericInput = function () {
	    _.forEach(inputTypes.onlyNumericInput, function (value, key) {
	        if (value === null) {
	            console.log("ignored: " + key);
	            return;
	        }
	        if (req.checkBody(key, "Please check your input").isNumeric()) {
	            if (key === "postcode" || key === "postalPostcode") {
	                req.checkBody(key, "Post code should not be more than 5 numbers").isLength({
	                    min: 4,
	                    max: 5
	                });
	            } else if (key === "primaryPhone" || key === "emergencyContactNumber") {
	                req.checkBody(key, "Please check the phone number you entered").isMobilePhone("en-AU");
	            }

	        }
	    })
	} */


	/**
	     * Method loops through all inputs where spaces IS ALLOWED to make sure this has not been completed by a bot
    
	    var validateDateInput = function () {
	        _.forEach(inputTypes.onlyDateInput, function (value, key) {
	            req.checkBody(key, "Please check the date you entered").isDate();
	            if (key === "dateOfBirth") {

	            }
	        });
	    } */

	/* Transform radio options to boolean 
	var booleanize = function () {
	    _.forEach(inputTypes.booleanInput, function (value, key) {
	        if (value === null) {
	            return;
	        }
	        if (value === "yes") {
	            req.body[key] = true;
	        } else if (value === "no") {
	            req.body[key] = undefined
	        } else {
	            req.body[key] = false;
	        }
	    })
	}*/

	var prepInputLogic = function () {

		/* Method returns boolean if applicant is eligible for VFH */
		var isEligibleForVfh = function () {
			return (cleanStr(req.body.citizenship.country === cleanStr("Australia")) || (cleanStr(req.body.citizenship.country === cleanStr("New Zealand")) && req.body.citizenship.scvVisa))
		}

		/* If person has no previous name, do not require previous name fields */
		if (!rawData.personal.hasPreviousName) {
			req.checkBody(["personal.previousFirstName", "personal.previousLastName"]).optional({
				checkFalsy: true
			});
		}

		/* If person has same postal address, do not require postal address fields */
		if (rawData.contact.address.same) {
			nullify([
				req.body.contact.address.postal.street,
				req.body.contact.address.postal.postcode,
				req.body.contact.address.postal.state,
				req.body.contact.address.postal.suburb
			]);
		}

		/* If country of birth is not Australia, require arrival date. Otherwise require cultural background */
		if (cleanStr(rawData.personal.countryOfBirth) !== cleanStr("Australia")) {
			nullify(req.body.personal.culturalBackground)
		} else {
			nullify(req.body.personal.yearOfArrival)
		}

		/* If did not attend high school, do not require associated fields */
		if (cleanStr(rawData.educational.highestLevel) === cleanStr("Never Attended School")) {
			nullify([
				req.body.educational.highSchool.name,
				req.body.educational.highSchool.state,
				req.body.educational.highSchool.year
			]);
		}
		/* If never employed, don't require employment details, otherwise require employment details as necessary */
		if (cleanStr(rawData.employment.employmentStatus) !== cleanStr("Full time employee") || cleanStr(rawData.employment.employmentStatus) !== cleanStr("Part time employee")) {
			nullify([
				req.body.employment.occupation,
				req.body.employment.industry
			]);
		}

		/* If not disabled, don't require disability details */
		if (!rawData.other.isDisabled) {
			nullify([
				req.body.other.disabilityType,
				req.body.other.hasDisabilityPlan,
				req.body.other.disabilityManagementPlan
			]);
		}

		if (!isEligibleForVfh()) {
			nullify([
				req.body.funding.vfh.eligible,
				req.body.funding.vfh.wantsToAccess,
				req.body.funding.vfh.hasRecentProvider,
				req.body.funding.vfh.recentProvider.name,
				req.body.funding.vfh.recentProvider.yearLastEnrolled,
				req.body.funding.vfh.recentProvider.studentNumber,
				req.body.funding.vfh.previouslyAccessedVfh,
				req.body.funding.vfh.CHESSN
			]);
		} else {
			var optionalList = [
				req.body.funding.vfh.recentProvider.name,
				req.body.funding.vfh.recentProvider.yearLastEnrolled,
				req.body.funding.vfh.recentProvider.studentNumber,
				req.body.funding.vfh.CHESSN
			],
				i = 0;
			for (i = 0; i < optionalList.length; i++) {
				if (optionalList[i] === "") {
					optionalList[i] = null;
				};
			}
		}

		this.funding = {
				vfh: {
					eligible: null,
					wantsToAccess: null,
					hasRecentProvider: null,
					recentProvider: {
						name: "",
						yearLastEnrolled: "",
						studentNumber: ""
					},
					previouslyAccessedVfh: null,
					CHESSN: ""
				}
			}
			/* LUI 
			if (cleanStr(req.body.attendingSchool) === "yes" && cleanStr(inputTypes.onlyStringInput.noSpacesAllowed.schoolState) === "QLD") {
			    inputTypes.onlyStringInput.noSpacesAllowed.lui = null;
			}*/

		return true;
	}

	/* Run validation after form logic is complete */
	if (prepInputLogic()) {

		console.log("Data validated successfully");
		/*validateBoolean();*/
		/*validatePersonalInfo();*/
		validateNoSpaces();
		validateSpacesAllowed();
		/* validateNumericInput();
		 validateDateInput();
		 booleanize();*/
	}

	var error = req.validationErrors();
	if (error) {
		console.log(error);
		return res.status(400).send({
			success: false,
			message: "Please make sure you've entered all details correctly."
		});
	}

	next();

}

module.exports = Validator;